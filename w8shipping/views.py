from django.views.generic import TemplateView
from django.urls import reverse_lazy
from .models import Marka, Model
from django.shortcuts import render


class MarkaView(TemplateView):
    model = Marka
    template_name = 'index.html'
    success_url = reverse_lazy('search_cars')

    def get_context_data(self, **kwargs):
        context = super(MarkaView, self).get_context_data(**kwargs)
        context['marka'] = Marka.objects.all().order_by('marka')
        context['year_from'] = ['2007', '2008', '2009']
        return context


def load_models(request):
    id_marka = request.GET.get('marka')
    car_models = Model.objects.filter(id_marka=id_marka)
    return render(request, 'partials/models_options.html', {'models': car_models})


def car_search(request):
    cars = Model.objects.filter()
    return render(request, 'base.html', context={'cars': cars})