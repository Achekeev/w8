from django import forms
from w8shipping.models import Marka, Model, Carscom, Iaaicom, Manheimcom, Copartcom


class MarkaSearchForm(forms.ModelChoiceField):
    name = forms.ModelChoiceField(
        queryset=Marka.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = Marka
        fields = ['model', 'marka', 'year_min', 'year_max']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['model', 'marka', 'year_min', 'year_max'].queryset = Model.objects.none()

        if 'marka' in self.data:
            try:
                id_marka = int(self.data.get('marka'))
                self.fields['model'].queryset = Model.objects.filter(id_marka=id_marka).order_by('model')
            except(ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['city'].queryset = self.instance.marka.model_set.order_by('model')


class CarscomSearchForm(forms.ModelChoiceField):
    name = forms.ModelChoiceField(
        queryset=Carscom.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'},)
    )

    class Meta:
        model = Carscom
        fields = ['year']


class ManheimSearchForm(forms.ModelChoiceField):
    name = forms.ModelChoiceField(
        queryset=Manheimcom.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Manheimcom
        fields = ['year']


class IaaicomSearchForm(forms.ModelChoiceField):
    name = forms.ModelChoiceField(
        queryset=Iaaicom.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Iaaicom
        fields = ['year']


class CopartcomSearchForm(forms.ModelChoiceField):
    name = forms.ModelChoiceField(
        queryset=Copartcom.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Copartcom
        fields = ['year']