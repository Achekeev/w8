# from django.contrib import admin
# from .models import Marka, Model
#
#
# class MarkaAdmin(admin.ModelAdmin):
#     class Meta:
#         model = Marka
#
#     fields = ('Marka',)
#     list_display = ('Marka',)
#
#
# class ModelAdmin(admin.ModelAdmin):
#     class Meta:
#         model = Model
#
#     fields = ('Marka', 'Model',)
#     list_display = ('Marka', 'Model',)
#
#
# admin.site.register(Marka, MarkaAdmin)
# admin.site.register(Model, ModelAdmin)
# # Register your models here.
