from django.db import models


class Marka(models.Model):
    marka = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Marka'

    def __str__(self):
        return self.marka


class Model(models.Model):
    id_marka = models.ForeignKey(Marka, models.DO_NOTHING, db_column='id_marka', blank=True, null=True)
    model = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Model'

    def __str__(self):
        return f"{self.id_marka.model} - {self.model} "


class Carscom(models.Model):
    id_marka = models.ForeignKey('Marka', models.DO_NOTHING, db_column='id_marka', blank=True, null=True)
    id_model = models.ForeignKey('Model', models.DO_NOTHING, db_column='id_model', blank=True, null=True)
    marka = models.CharField(max_length=600, blank=True, null=True)
    model = models.CharField(max_length=600, blank=True, null=True)
    car_info = models.CharField(max_length=600, blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    url = models.CharField(max_length=1000, blank=True, null=True)
    info = models.CharField(max_length=1500, blank=True, null=True)
    basics = models.CharField(max_length=1500, blank=True, null=True)
    fuel_type = models.CharField(max_length=200, blank=True, null=True)
    exterior_color = models.CharField(max_length=200, blank=True, null=True)
    city_mpg = models.CharField(db_column='city_MPG', max_length=200, blank=True, null=True)  # Field name made lowercase.
    interior_color = models.CharField(max_length=200, blank=True, null=True)
    highway_mpg = models.CharField(db_column='highway_MPG', max_length=200, blank=True, null=True)  # Field name made lowercase.
    stock = models.CharField(max_length=200, blank=True, null=True)
    drivetrain = models.CharField(max_length=200, blank=True, null=True)
    transmission = models.CharField(max_length=200, blank=True, null=True)
    engine1 = models.CharField(max_length=200, blank=True, null=True)
    vin = models.CharField(max_length=200, blank=True, null=True)
    mileage = models.CharField(max_length=200, blank=True, null=True)
    parse_data = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CarsCom'


class Carsphoto(models.Model):
    id_cars = models.ForeignKey(Carscom, models.DO_NOTHING, db_column='id_cars', blank=True, null=True)
    url = models.CharField(max_length=15000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CarsPhoto'


class Copartcom(models.Model):
    id_marka = models.ForeignKey('Marka', models.DO_NOTHING, db_column='id_marka', blank=True, null=True)
    id_model = models.ForeignKey('Model', models.DO_NOTHING, db_column='id_model', blank=True, null=True)
    marka = models.CharField(max_length=500, blank=True, null=True)
    model = models.CharField(max_length=500, blank=True, null=True)
    url = models.CharField(max_length=1200, blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    lot = models.CharField(max_length=500, blank=True, null=True)
    doc_type = models.CharField(max_length=500, blank=True, null=True)
    odometr = models.CharField(max_length=500, blank=True, null=True)
    highlights = models.CharField(max_length=500, blank=True, null=True)
    primary_damage = models.CharField(max_length=500, blank=True, null=True)
    second_damage = models.CharField(max_length=500, blank=True, null=True)
    body_style = models.CharField(max_length=500, blank=True, null=True)
    color = models.CharField(max_length=500, blank=True, null=True)
    engine_type = models.CharField(max_length=500, blank=True, null=True)
    cylinders = models.CharField(max_length=500, blank=True, null=True)
    driver = models.CharField(max_length=500, blank=True, null=True)
    keys = models.CharField(max_length=500, blank=True, null=True)
    sale_date = models.CharField(max_length=500, blank=True, null=True)
    vin = models.CharField(max_length=500, blank=True, null=True)
    price = models.CharField(max_length=500, blank=True, null=True)
    repair = models.CharField(max_length=500, blank=True, null=True)
    parse_data = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CopartCom'


class Copartphoto(models.Model):
    id_copart = models.ForeignKey(Copartcom, models.DO_NOTHING, db_column='id_copart', blank=True, null=True)
    url = models.CharField(max_length=15000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CopartPhoto'


class Iaaiphoto(models.Model):
    id_iaai = models.ForeignKey('Iaaicom', models.DO_NOTHING, db_column='id_iaai', blank=True, null=True)
    url = models.CharField(max_length=15000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IaaIPhoto'


class Iaaicom(models.Model):
    id_marka = models.ForeignKey('Marka', models.DO_NOTHING, db_column='id_marka', blank=True, null=True)
    id_model = models.ForeignKey('Model', models.DO_NOTHING, db_column='id_model', blank=True, null=True)
    marka = models.CharField(max_length=600, blank=True, null=True)
    model = models.CharField(max_length=600, blank=True, null=True)
    url = models.CharField(max_length=800, blank=True, null=True)
    car_info = models.CharField(max_length=500, blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    stock = models.CharField(max_length=500, blank=True, null=True)
    loss = models.CharField(max_length=500, blank=True, null=True)
    primary_damage = models.CharField(max_length=500, blank=True, null=True)
    second_damage = models.CharField(max_length=500, blank=True, null=True)
    odometer = models.CharField(max_length=500, blank=True, null=True)
    start_code = models.CharField(max_length=500, blank=True, null=True)
    key_fob = models.CharField(max_length=500, blank=True, null=True)
    vehicle_wheels = models.CharField(max_length=500, blank=True, null=True)
    airbags = models.CharField(max_length=500, blank=True, null=True)
    selling_branc = models.CharField(max_length=500, blank=True, null=True)
    vehicle_location = models.CharField(max_length=500, blank=True, null=True)
    auction_date = models.CharField(max_length=500, blank=True, null=True)
    lane_item = models.CharField(max_length=500, blank=True, null=True)
    seller = models.CharField(max_length=500, blank=True, null=True)
    actual_price = models.CharField(max_length=500, blank=True, null=True)
    estimated_repair = models.CharField(max_length=500, blank=True, null=True)
    title_doc = models.CharField(max_length=500, blank=True, null=True)
    title_brand = models.CharField(max_length=500, blank=True, null=True)
    title_note = models.CharField(max_length=500, blank=True, null=True)
    vehicle = models.CharField(max_length=500, blank=True, null=True)
    manufactured = models.CharField(max_length=500, blank=True, null=True)
    body_style = models.CharField(max_length=500, blank=True, null=True)
    vehicle_class = models.CharField(max_length=500, blank=True, null=True)
    series = models.CharField(max_length=500, blank=True, null=True)
    engine = models.CharField(max_length=500, blank=True, null=True)
    fuel_type = models.CharField(max_length=500, blank=True, null=True)
    cylinders = models.CharField(max_length=500, blank=True, null=True)
    restraint_system = models.CharField(max_length=500, blank=True, null=True)
    transmission = models.CharField(max_length=500, blank=True, null=True)
    drive_line = models.CharField(max_length=500, blank=True, null=True)
    exterior_interior = models.CharField(max_length=500, blank=True, null=True)
    options = models.CharField(max_length=500, blank=True, null=True)
    parse_data = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IaaiCom'


class Manheimcom(models.Model):
    id_marka = models.ForeignKey('Marka', models.DO_NOTHING, db_column='id_marka', blank=True, null=True)
    id_model = models.ForeignKey('Model', models.DO_NOTHING, db_column='id_model', blank=True, null=True)
    marka = models.TextField(blank=True, null=True)
    model = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    auction_date = models.TextField(blank=True, null=True)
    trim_level = models.TextField(blank=True, null=True)
    rspec_code = models.TextField(blank=True, null=True)
    odometer = models.TextField(blank=True, null=True)
    in_service_date = models.TextField(blank=True, null=True)
    fuel_type = models.TextField(blank=True, null=True)
    engine = models.TextField(blank=True, null=True)
    displacement = models.TextField(blank=True, null=True)
    transmission = models.TextField(blank=True, null=True)
    exterior_color = models.TextField(blank=True, null=True)
    interior_olor = models.TextField(blank=True, null=True)
    window_sticker = models.TextField(blank=True, null=True)
    vin = models.TextField(blank=True, null=True)
    body_style = models.TextField(blank=True, null=True)
    doors = models.TextField(blank=True, null=True)
    vehicle_type = models.TextField(blank=True, null=True)
    salvage = models.TextField(blank=True, null=True)
    as_is = models.TextField(blank=True, null=True)
    title_state = models.TextField(blank=True, null=True)
    title_status = models.TextField(blank=True, null=True)
    drive_train = models.TextField(blank=True, null=True)
    interior_type = models.TextField(blank=True, null=True)
    top_type = models.TextField(blank=True, null=True)
    stereo = models.TextField(blank=True, null=True)
    airbags = models.TextField(blank=True, null=True)
    cr_options = models.TextField(blank=True, null=True)
    parse_data = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ManheimCom'


class Manheimphoto(models.Model):
    id_manheim = models.ForeignKey(Manheimcom, models.DO_NOTHING, db_column='id_manheim', blank=True, null=True)
    url = models.CharField(max_length=15000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ManheimPhoto'





class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
# Create your models here.
