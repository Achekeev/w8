from django.apps import AppConfig


class W8ShippingConfig(AppConfig):
    name = 'w8shipping'
